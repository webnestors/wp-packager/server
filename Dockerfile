FROM docker.io/golang:1.18-alpine AS build
WORKDIR /app

RUN apk add build-base

COPY go.mod ./
COPY go.sum ./

RUN go mod download

COPY *.go ./
COPY templates/* ./templates/
COPY migrations/* ./migrations/
RUN go build -tags=nomsgpack -o server .

FROM docker.io/alpine:latest

WORKDIR /app

COPY --from=build /app /app
ENV GIN_MODE=release

CMD ["./server"]
