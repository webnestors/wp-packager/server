## WP-Package-Mantle Server

A very simple go server that can provide to a client a *plugin info* JSON, a
download url, and a software license key.

### Features

- Simple REST API
- Plugin Info JSON for your plugin's information data
- Upload and download plugin packages
- Generate and verify license keys for premium/pro plugins
