CREATE TABLE IF NOT EXISTS plugins (
  id              TEXT PRIMARY KEY,
  requiresLicense INT,
  pluginInfo
);

CREATE TABLE IF NOT EXISTS licenseEntries (
  id       TEXT PRIMARY KEY,
  email    TEXT,
  license  TEXT,
  pluginId TEXT NULL,
  themeId  TEXT NULL,
  expires  INT,
  revoked  INT
);
