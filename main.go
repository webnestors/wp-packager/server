package main

import (
	"archive/zip"
	"bytes"
	"encoding/json"
	"io"
	"log"
	"net/http"
	"os"
	"path/filepath"
	"strconv"
	"time"

	"github.com/golang-migrate/migrate/v4"
	"github.com/golang-migrate/migrate/v4/database/sqlite3"
	_ "github.com/golang-migrate/migrate/v4/source/file"
	"github.com/google/uuid"
	_ "github.com/mattn/go-sqlite3"

	"github.com/gin-gonic/gin"

	"github.com/jmoiron/sqlx"
	"github.com/jmoiron/sqlx/types"
)

type config struct {
	Authentication struct {
		Username string
		Password string
	}
	BindPort       string
	DataDir        string
	LogFile        string
	PrivateKey     string
	TrustedProxies []string
}

type pluginInfo struct {
	Name           string         `json:"name"`
	Author         string         `json:"author"`
	AuthorHomepage string         `json:"author_homepage"`
	Version        string         `json:"version"`
	DownloadUrl    string         `json:"download_url"`
	Homepage       string         `json:"homepage"`
	Requires       string         `json:"requires"`
	RequiresPhp    string         `json:"requires_php"`
	Tested         string         `json:"tested"`
	LastUpdated    string         `json:"last_updated"`
	UpgradeNotice  string         `json:"upgrade_notice"`
	Sections       types.JSONText `json:"sections"`
	Icons          types.JSONText `json:"icons"`
	Banners        types.JSONText `json:"banners"`
}

// DB: plugins
type plugin struct {
	Id              string         `db:"id"`
	RequiresLicense bool           `db:"requiresLicense"`
	PluginInfo      types.JSONText `db:"pluginInfo"`
}

// DB: licenseEntries
type licenseEntry struct {
	Id       string  `json:"-" db:"id"`
	Email    string  `json:"email" db:"email"`
	License  string  `json:"-" db:"license"`
	PluginId *string `json:"plugin_id" db:"pluginId"`
	ThemeId  *string `json:"theme_id" db:"themeId"`
	Expires  int64   `json:"expires" db:"expires"`
	Revoked  bool    `json:"-" db:"revoked"`
}

var db *sqlx.DB
var cfg config

func getPlugin(pluginId string) *plugin {
	var p plugin

	if err := db.Get(&p, "select * from plugins where id = $1", pluginId); err != nil {
		return nil
	}

	return &p
}

func postPlugin(pluginId string, info pluginInfo, requiresLicense bool) error {
	infoData, _ := json.Marshal(info)

	_, err := db.Exec(`insert into plugins (id, requiresLicense, pluginInfo)
		values(?,?,?)
		on conflict(id)
		do update set pluginInfo = ?, requiresLicense = ?`,
		pluginId, requiresLicense, infoData, infoData, requiresLicense)

	return err
}

func getAllPlugins() (plugins []plugin) {
	db.Select(&plugins, "select * from plugins")
	return plugins
}

func postLicenseEntry(license *licenseEntry) error {
	license.Id = uuid.New().String()
	key := generateLicense(license.Id)
	license.License = key
	_, err := db.Exec(`insert into licenseEntries (id, email, license, pluginId, expires, revoked)
		values(?,?,?,?,?,?)`, license.Id, license.Email, license.License, license.PluginId, license.Expires, false)
	return err
}

func placeLicenseKeyInArchive(z *zip.ReadCloser, w io.Writer, license string) error {
	zw := zip.NewWriter(w)
	defer zw.Close()

	for _, item := range z.File {
		r, err := item.Open()
		if err != nil {
			return err
		}

		b, err := io.ReadAll(r)
		if err != nil {
			return err
		}

		newB := bytes.ReplaceAll(b, []byte("##WP-Packager-License##"), []byte(license))
		target, _ := zw.Create(item.Name)
		target.Write(newB)
	}
	return nil
}

func validateLicense(license string) (*licenseEntry, bool) {
	entry := &licenseEntry{}
	data, verified := verifyLicense(license)

	var id string
	if err := decodePayload(&id, data); err != nil {
		return nil, false
	}

	db.Get(entry, "select * from licenseEntries where id = ?", id)

	return entry, verified && entry.Expires > time.Now().Unix() && !entry.Revoked
}

func renewLicense(entry licenseEntry, expires int64) error {
	_, err := db.Exec("update licenseEntries set expires = ?", expires)
	return err
}

func setupRouter() *gin.Engine {
	r := gin.Default()
	r.SetTrustedProxies(cfg.TrustedProxies)
	r.LoadHTMLGlob("templates/*")

	accounts := make(gin.Accounts)
	accounts[cfg.Authentication.Username] = cfg.Authentication.Password
	authorized := r.Group("/")
	authorized.Use(gin.BasicAuth(accounts))

	{
		admin := authorized.Group("/admin")
		admin.GET("/", func(c *gin.Context) {
			plugins := getAllPlugins()
			c.HTML(http.StatusOK, "index.tmpl", gin.H{
				"plugins": plugins,
			})
		})
	}

	// Get plugin info
	r.GET("/plugin/:id", func(c *gin.Context) {
		id := c.Params.ByName("id")
		plugin := getPlugin(id)

		if plugin == nil {
			c.AbortWithStatusJSON(http.StatusNotFound, gin.H{
				"status": "not found",
			})
		} else {
			c.JSON(http.StatusOK, plugin.PluginInfo)
		}
	})

	// Create a plugin with plugin info
	authorized.POST("/plugin/:id", func(c *gin.Context) {
		id := c.Params.ByName("id")
		requiresLicense, _ := strconv.ParseBool(c.Query("requiresLicense"))

		var info pluginInfo
		if err := c.ShouldBind(&info); err != nil {
			c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{
				"status": err.Error(),
			})
		} else if err := postPlugin(id, info, requiresLicense); err != nil {
			c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{
				"status": err.Error(),
			})
		}
	})

	// Upload a downloadable zip file for given plugin id
	authorized.PUT("/plugin/:id", func(c *gin.Context) {
		id := c.Params.ByName("id")

		file, err := c.FormFile("file")
		if err != nil {
			c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{
				"status": err.Error(),
			})
			return
		}

		if matched, _ := filepath.Match("*.zip", filepath.Base(file.Filename)); !matched {
			c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{
				"status": "file must be a zip file",
			})
			return
		}

		path := filepath.Join(cfg.DataDir, "plugins", id+".zip")
		err = c.SaveUploadedFile(file, path)
		if err != nil {
			c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{
				"status": err.Error(),
			})
			return
		}
	})

	authorized.POST("/plugin/generateLicense", func(c *gin.Context) {
		var l licenseEntry
		if err := c.ShouldBind(&l); err != nil {
			c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{
				"status": err.Error(),
			})
			return
		}

		if err := postLicenseEntry(&l); err != nil {
			c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{
				"status": err.Error(),
			})
			return
		}

		c.JSON(http.StatusOK, gin.H{
			"key": l.License,
		})
	})

	authorized.POST("/plugin/renewLicense", func(c *gin.Context) {
		var body struct {
			License string
			Expires int64
		}
		if err := c.ShouldBind(&body); err != nil {
			c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{
				"status": err.Error(),
			})
			return
		}

		lbytes, valid := verifyLicense(body.License)
		if !valid {
			c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{
				"status": "license is not valid",
			})
			return
		}

		var l licenseEntry
		decodePayload(&l, lbytes)

		if err := renewLicense(l, body.Expires); err != nil {
			c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{
				"status": err.Error(),
			})
			return
		}
	})

	r.GET("/plugin/:id/download", func(c *gin.Context) {
		id := c.Params.ByName("id")
		license := c.GetHeader("WP-License")

		plugin := getPlugin(id)

		if plugin == nil {
			c.AbortWithStatusJSON(http.StatusNotFound, gin.H{
				"status": "plugin not found",
			})
			return
		}

		filepath := filepath.Join(cfg.DataDir, "plugins", id+".zip")
		if _, err := os.Stat(filepath); err != nil {
			c.AbortWithStatusJSON(http.StatusNotFound, gin.H{
				"status": "download file not found",
			})
			return
		}

		if plugin.RequiresLicense {
			l, valid := validateLicense(license)
			if !valid || *l.PluginId != id {
				c.AbortWithStatusJSON(http.StatusUnauthorized, gin.H{
					"status": "license is invalid, expired, or not for this package",
				})
				return
			}

			r, _ := zip.OpenReader(filepath)
			defer r.Close()

			w, _ := os.CreateTemp(os.TempDir(), plugin.Id)
			defer w.Close()
			filepath = w.Name() // Replace filepath

			if err := placeLicenseKeyInArchive(r, w, license); err != nil {
				c.AbortWithStatusJSON(http.StatusUnauthorized, gin.H{
					"status": "there was an error producing the zip file",
				})
				log.Println(err)
				return
			}
		}

		c.FileAttachment(filepath, id+".zip")
	})

	r.POST("/plugin/verifyLicense", func(c *gin.Context) {
		var license struct {
			License string
		}
		if err := c.ShouldBind(&license); err != nil {
			c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{
				"status": "missing license",
			})
			return
		}

		_, valid := validateLicense(license.License)
		c.JSON(http.StatusOK, gin.H{
			"valid": valid,
		})
	})

	return r
}

func loadConfig() {
	file, err := os.Open("config.json")
	if err != nil {
		log.Fatalf("could not load config.json: %v", err)
	}
	defer file.Close()

	err = json.NewDecoder(file).Decode(&cfg)
	if err != nil {
		log.Fatalf("could not load config.json: %v", err)
	}
}

func connectDb(dbname string) *sqlx.DB {
	conn, err := sqlx.Connect("sqlite3", filepath.Join(cfg.DataDir, dbname))
	if err != nil {
		log.Fatalln(err)
	}

	driver, err := sqlite3.WithInstance(conn.DB, &sqlite3.Config{})
	if err != nil {
		log.Fatalln(err)
	}

	m, err := migrate.NewWithDatabaseInstance("file://migrations", "sqlite3", driver)
	if err != nil {
		log.Fatalln(err)
	}
	m.Up()

	return conn
}

func main() {
	loadConfig()
	db = connectDb("db.sqlite3")
	defer db.Close()
	r := setupRouter()
	r.Run(":" + cfg.BindPort)
}
