package main

import (
	"archive/zip"
	"bufio"
	"bytes"
	"encoding/base64"
	"io"
	"mime/multipart"
	"net/http"
	"net/http/httptest"
	"os"
	"strings"
	"testing"

	"github.com/gin-gonic/gin"
	"github.com/goccy/go-json"
	"github.com/jmoiron/sqlx/types"
	"github.com/stretchr/testify/assert"
)

func TestGetPluginRoute(t *testing.T) {
	loadConfig()
	db = connectDb("test_getplugin.sqlite3")
	defer db.Close()

	r := setupRouter()

	w := httptest.NewRecorder()
	req, _ := http.NewRequest(http.MethodGet, "/plugin/test", nil)
	r.ServeHTTP(w, req)

	var b string
	json.NewDecoder(w.Body).Decode(&b)
	assert.Equal(t, http.StatusNotFound, w.Code)
	assert.Equal(t, b, w.Body.String())
}

func TestPostPluginRoute(t *testing.T) {
	loadConfig()
	db = connectDb("test_postplugin.sqlite3")
	defer db.Close()

	r := setupRouter()

	info := pluginInfo{
		Name:           "Test",
		Author:         "TestAuthor",
		AuthorHomepage: "https://test.test/",
		Version:        "1.0.0",
		DownloadUrl:    "https://packages.test.test/plugin/test/download/",
		Homepage:       "https://test.test/",
		Requires:       "5.6",
		RequiresPhp:    "7.4",
		Tested:         "6.0",
		LastUpdated:    "2022-06-08 10:00:00",
		UpgradeNotice:  "",
		Sections:       types.JSONText{},
		Icons:          types.JSONText{},
		Banners:        types.JSONText{},
	}

	infoBytes, _ := json.Marshal(info)
	reader := bytes.NewReader(infoBytes)

	// Test unauthorized
	w := httptest.NewRecorder()
	req, _ := http.NewRequest(http.MethodPost, "/plugin/test", reader)
	req.Header.Add("Content-Type", "application/json")
	r.ServeHTTP(w, req)

	assert.Equal(t, http.StatusUnauthorized, w.Code)

	// Test authorized
	w = httptest.NewRecorder()
	creds := base64.StdEncoding.EncodeToString([]byte("changeme:changeme"))
	req.Header.Add("Authorization", "Basic "+creds)
	r.ServeHTTP(w, req)

	assert.Equal(t, http.StatusOK, w.Code)

	// Test get plugin info
	w = httptest.NewRecorder()
	req, _ = http.NewRequest(http.MethodGet, "/plugin/test", nil)
	r.ServeHTTP(w, req)

	assert.Equal(t, http.StatusOK, w.Code)
	assert.Equal(t, infoBytes, w.Body.Bytes())
}

func TestPutPluginRoute(t *testing.T) {
	loadConfig()
	db = connectDb("test_putplugin.sqlite3")
	defer db.Close()

	r := setupRouter()

	pr, pw := io.Pipe()
	writer := multipart.NewWriter(pw)

	go func() {
		defer writer.Close()

		part, _ := writer.CreateFormFile("file", "test.zip")

		file, err := os.ReadFile("examples/test.zip")
		if err != nil {
			t.Error(err)
		}

		part.Write(file)
	}()

	// unauthorized
	w := httptest.NewRecorder()
	req, _ := http.NewRequest(http.MethodPut, "/plugin/test", pr)
	req.Header.Add("Content-Type", writer.FormDataContentType())
	r.ServeHTTP(w, req)

	assert.Equal(t, http.StatusUnauthorized, w.Code, w.Body.String())

	// authorized
	w = httptest.NewRecorder()
	creds := base64.StdEncoding.EncodeToString([]byte("changeme:changeme"))
	req.Header.Add("Authorization", "Basic "+creds)
	r.ServeHTTP(w, req)

	assert.Equal(t, http.StatusOK, w.Code, w.Body.String())
	_, err := os.Stat("data/plugins/test.zip")
	assert.NoError(t, err)
}

func TestGenerateLicenseRoute(t *testing.T) {
	loadConfig()
	db = connectDb("test_postplugin.sqlite3")
	defer db.Close()

	r := setupRouter()
	id := "test"

	entry := licenseEntry{
		Id:       "test-id",
		PluginId: &id,
		Email:    "nikos@test.test",
		Expires:  1686465252,
	}

	entryBytes, _ := json.Marshal(entry)
	reader := bytes.NewReader(entryBytes)

	// Test license
	w := httptest.NewRecorder()
	creds := base64.StdEncoding.EncodeToString([]byte("changeme:changeme"))
	req, _ := http.NewRequest(http.MethodPost, "/plugin/generateLicense", reader)
	req.Header.Add("Content-Type", "application/json")
	req.Header.Add("Authorization", "Basic "+creds)
	r.ServeHTTP(w, req)

	assert.Equal(t, http.StatusOK, w.Code)
}

func TestVerifyLicenseRoute(t *testing.T) {
	loadConfig()
	db = connectDb("test_postplugin.sqlite3")
	defer db.Close()

	r := setupRouter()

	key := generateLicense("test-id")
	license := gin.H{
		"License": key,
	}
	b, _ := json.Marshal(license)
	reader := bytes.NewReader(b)

	w := httptest.NewRecorder()
	req, _ := http.NewRequest(http.MethodPost, "/plugin/verifyLicense", reader)
	req.Header.Add("Content-Type", "application/json")
	r.ServeHTTP(w, req)

	assert.Equal(t, http.StatusOK, w.Code)
}

func TestRenewLicenseRoute(t *testing.T) {
	loadConfig()
	db = connectDb("test_postplugin.sqlite3")
	defer db.Close()

	r := setupRouter()

	key := generateLicense("test-id")
	body := gin.H{
		"License": key,
		"Expires": 2686465252,
	}
	b, _ := json.Marshal(body)
	reader := bytes.NewReader(b)

	w := httptest.NewRecorder()
	creds := base64.StdEncoding.EncodeToString([]byte("changeme:changeme"))
	req, _ := http.NewRequest(http.MethodPost, "/plugin/renewLicense", reader)
	req.Header.Add("Content-Type", "application/json")
	req.Header.Add("Authorization", "Basic "+creds)
	r.ServeHTTP(w, req)

	l := licenseEntry{}
	err := db.Get(&l, "select * from licenseEntries")

	assert.Equal(t, http.StatusOK, w.Code)
	assert.NoError(t, err)
	assert.Equal(t, int64(2686465252), l.Expires)
}

func TestPlaceLicenseKeyInArchive(t *testing.T) {
	r, _ := zip.OpenReader("examples/test.zip")
	defer r.Close()
	w, _ := os.CreateTemp(os.TempDir(), "wp-packager-test-*.zip")
	defer w.Close()
	err := placeLicenseKeyInArchive(r, w, "test-license-key")
	assert.NoError(t, err)

	found := false
	zr, _ := zip.OpenReader(w.Name())
	for _, f := range zr.File {
		item, _ := f.Open()
		sc := bufio.NewScanner(item)
		for sc.Scan() {
			if strings.Contains(sc.Text(), "test-license-key") {
				found = true
				break
			}
		}
	}
	assert.True(t, found)
}
