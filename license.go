package main

import (
	"bytes"
	"crypto/hmac"
	"crypto/sha256"
	"encoding/base64"
	"encoding/gob"
	"log"
	"strings"
)

type license struct {
	Payload   []byte
	Signature []byte
}

func generateLicense(payload any) string {
	h := hmac.New(sha256.New, []byte(cfg.PrivateKey))

	pbuf := bytes.Buffer{}
	encoder := gob.NewEncoder(&pbuf)
	if err := encoder.Encode(payload); err != nil {
		log.Println(err)
	}
	h.Write(pbuf.Bytes())

	l := license{
		Payload:   pbuf.Bytes(),
		Signature: h.Sum(nil),
	}

	lbuf := bytes.Buffer{}
	encoder = gob.NewEncoder(&lbuf)
	if err := encoder.Encode(l); err != nil {
		log.Println(err)
	}

	return base64.StdEncoding.EncodeToString(lbuf.Bytes())
}

func verifyLicense(b64 string) ([]byte, bool) {
	b64 = strings.TrimSpace(b64)

	var l license
	b, _ := base64.StdEncoding.DecodeString(b64)
	lbuf := bytes.Buffer{}
	lbuf.Write(b)
	decoder := gob.NewDecoder(&lbuf)
	if err := decoder.Decode(&l); err != nil {
		return nil, false
	}

	h := hmac.New(sha256.New, []byte(cfg.PrivateKey))
	h.Write(l.Payload)

	return l.Payload, hmac.Equal(l.Signature, h.Sum(nil))
}

func decodePayload(dst any, src []byte) error {
	buf := bytes.Buffer{}
	buf.Write(src)
	decoder := gob.NewDecoder(&buf)
	err := decoder.Decode(dst)
	return err
}
